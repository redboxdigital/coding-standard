<?php

namespace RedboxM2\Sniffs\Templates;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

class ObjectManagerInTemplateSniff implements Sniff
{

    protected $message = 'Direct usage of object manager in template files is forbidden!';


    public function register()
    {
        return [
            T_DOUBLE_COLON
        ];
    }

    public function process(File $phpcsFile, $stackPtr)
    {
        $parentClassNamePosition = $phpcsFile->findNext([T_STRING], $stackPtr);
        $tokens = $phpcsFile->getTokens();
        $parentClassNameToken = $tokens[$parentClassNamePosition];
        if (stripos($parentClassNameToken['content'], 'getInstance')
            !== false
            && $tokens[$parentClassNamePosition - 2]['content']
            === "ObjectManager"
        ) {
            $phpcsFile->addError(
                $this->message, $stackPtr, 'ObjectManagerInTemplateSniff'
            );
        }
        return;
    }
}
