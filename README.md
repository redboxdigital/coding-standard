# Redbox Digital Magento Code Sniffer Coding Standard


Redbox Digital Magento Code Sniffer Coding Standard is a set of rules and sniffs for [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) tool based on [Magento ECG rule set.](https://github.com/magento-ecg/coding-standard)  

In addition to all the checks provided by Magento ECG and PHP coding standards, it includes the below;  

- Direct usage of object manager in phtml files.  
- PSR1 checks  
- PSR2 Checks   

and many others.   

Only Magento 2 is supported for now.  

# Installation & Usage  

Before starting using our coding standard install [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer).

Clone or download this repo somewhere on your computer or install it with [Composer](http://getcomposer.org/):

```sh
composer require redboxdigital/coding-standard
```

Note: PHP_CodeSniffer 3.x is now requred to run our coding standard. To install PHP_CodeSniffer 2.x compatible version:

```sh
composer require redboxdigital/coding-standard:dev-master
```

Select a standard to run with CodeSniffer:

* RedboxM2 for Magento 2

Run CodeSniffer:

```sh
$ phpcs --standard=./vendor/redboxdigital/coding-standard/RedboxM2 /path/to/code
```

As a one time thing, you can add the ECG standards directory to PHP_CodeSniffer's installed paths:
```sh
$ phpcs --config-set installed_paths /path/to/your/folder/vendor/redboxdigital/coding-standard
```

After that specifying the path to a standard is optional:
```sh
$ phpcs --standard=RedboxM2 /path/to/code
```

>PHP CodeSniffer will automatically scan Magento PHP files. To check design templates, you must specify `phtml` in the `--extensions` argument: `--extensions=php,phtml`.

# Requirements

PHP 5.4 and up.

Checkout the `php-5.3-compatible` branch to get the PHP 5.3 version.